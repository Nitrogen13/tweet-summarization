import re
import nltk
import pandas
import preprocessor as tweet_prep  # pip install tweet-preprocessor
from src.constants import DATA_PATH, STOP_WORDS

ps = nltk.stem.PorterStemmer()


def get_query_results_file(query: str):
    r = re.compile("[a-zA-Z0-9]+")
    file_name = " ".join(r.findall(query)) + ".txt"
    return open(DATA_PATH / 'raw_tweets' / file_name, 'w', encoding="UTF-8")


def tokenize(text, use_stemmer=False, vocabulary=None):
    return [ps.stem(token) if use_stemmer else token
            for token in nltk.word_tokenize(text)
            if token not in STOP_WORDS
            and (vocabulary is None or token in vocabulary)]


def clean_original(text):
    tweet_prep.set_options(tweet_prep.OPT.URL, tweet_prep.OPT.RESERVED, tweet_prep.OPT.MENTION)
    cleaned = tweet_prep.clean(text)
    return cleaned.replace("&amp;", "&")


def preprocess_tweet(text):
    tweet_prep.set_options(tweet_prep.OPT.URL, tweet_prep.OPT.RESERVED, tweet_prep.OPT.EMOJI,
                           tweet_prep.OPT.SMILEY, tweet_prep.OPT.NUMBER, tweet_prep.OPT.MENTION)
    cleaned = tweet_prep.clean(text)
    cleaned = re.sub('[#]', '', cleaned).lower()
    return cleaned.replace("&amp;", "&")


def get_topic_contents(topic_file_name):
    with open(topic_file_name, "r", encoding="utf-8") as topic_file:
        data = pandas.read_csv(topic_file, sep="\n", header=None, names=["original"])

    data = data[data["original"].apply(lambda t: t[:2]) != 'RT']  # remove retweets
    data["processed"] = data["original"].apply(preprocess_tweet)  # preprocess tweets
    data["original"] = data["original"].apply(clean_original)  # preprocess tweets
    data = data.drop_duplicates("processed")  # remove duplicated tweets
    # TODO remove too short tweets
    return data
