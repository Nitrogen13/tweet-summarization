from sklearn.preprocessing import normalize
from src import preprocessing
import numpy as np
from pyemd import emd
import gensim.downloader
from gensim.models import KeyedVectors
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import pairwise_distances

VOCABULARY_LIMIT = 50000
EMBEDDINGS_MODEL_PATH = gensim.downloader.load("glove-twitter-25", return_path=True)

model = None


def get_wmd_matrix(tweets):
    # Lazy initialisation of model
    global model
    if model is None:
        print("Loading model...")
        model = KeyedVectors.load_word2vec_format(EMBEDDINGS_MODEL_PATH, limit=VOCABULARY_LIMIT)

    # Create histograms
    vectorizer = CountVectorizer(tokenizer=lambda text: preprocessing.tokenize(text, vocabulary=model))
    histograms = normalize(vectorizer.fit_transform(tweets).astype(np.float64), norm="l1", axis=1)
    vocabulary = vectorizer.vocabulary_

    # Create word distance matrix
    embeddings = [model[word] for word, index in sorted(vocabulary.items(), key=lambda item: item[1])]
    word_distances = pairwise_distances(embeddings, metric="euclidean", n_jobs=-1).astype(np.float64)

    # Calculate pairwise distance matrix
    sentence_distances = pairwise_distances(histograms, n_jobs=-1,
                                            metric=lambda doc1, doc2: wmd(doc1, doc2, word_distances))

    return sentence_distances


def wmd(doc1, doc2, distance_matrix):
    return emd(doc1.toarray().squeeze(), doc2.toarray().squeeze(), distance_matrix)
