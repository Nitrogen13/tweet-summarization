import os
from src import preprocessing, wmd
from src.lsa import lsa
from src.text_rank import text_rank
from src.constants import TWEETS_PATH, TOP_K_RESULTS


def wmd_text_rank_summary(tweets):
    distance_matrix = wmd.get_wmd_matrix(tweets[:50])
    return text_rank(distance_matrix, TOP_K_RESULTS)


def lsa_summary(tweets):
    return lsa(tweets, TOP_K_RESULTS)


def summarise_topic(topic_file_name, summarization_method):
    data = preprocessing.get_topic_contents(topic_file_name)
    tweet_ids = summarization_method(data["processed"])
    return data.iloc[tweet_ids]["original"]


def test_topic(topic_file):
    print(f"Summarizing {topic_file}:")

    print("LSA: ")
    lsa_result = summarise_topic(TWEETS_PATH / topic_file, lsa_summary)
    print("\n".join(lsa_result), "\n")

    print("WMD + TextRank: ")
    wmd_text_rank_result = summarise_topic(TWEETS_PATH / topic_file, wmd_text_rank_summary)
    print("\n".join(wmd_text_rank_result), "\n")


def test_all():
    for topic_file in os.listdir(TWEETS_PATH)[:5]:
        test_topic(topic_file)


if __name__ == '__main__':
    test_topic("EasterDay.txt")
    # test_all()
