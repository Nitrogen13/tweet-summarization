import time

from tweepy import AppAuthHandler, API, Cursor
import os

from src.constants import CONSUMER_SECRET, CONSUMER_KEY, DATA_PATH
from src.preprocessing import get_query_results_file

TWEETS_PER_REQUEST = 100
MAX_TWEETS_FOR_QUERY = 1000
ON_ERROR_SLEEP_SEC = 15 * 60


def query_through_search(queries):
    os.makedirs(DATA_PATH / 'raw_tweets', exist_ok=True)
    query_results_files = {query: get_query_results_file(query) for query in queries}
    results = {q: set() for q in queries}

    api = API(auth)

    while queries:
        for query in queries:
            print(f"Downloading tweets for query : {query}")
            try:
                new_tweets = api.search(q=query.encode('utf-8'), tweet_mode='extended', count=TWEETS_PER_REQUEST,
                                        lang="en", result_type="mixed", locale="en")
                for tweet in new_tweets:
                    tweet_text = tweet.full_text
                    if tweet.user.followers_count > 0 and tweet_text not in results[query]:
                        results[query].add(tweet_text)
                        query_results_files[query].write(tweet_text.replace('\n', " ") + '\n')

                print("Downloaded tweets: %s" % {query: len(result) for query, result in results.items()})
            except Exception as e:
                print(f"{time.asctime()} Error: {str(e)}")
                time.sleep(ON_ERROR_SLEEP_SEC)

        queries = [q for q in queries if len(results[q]) < MAX_TWEETS_FOR_QUERY]

    for file in query_results_files.values():
        file.close()


if __name__ == '__main__':
    auth = AppAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    tags = [tag.strip() for tag in open(DATA_PATH / 'trending_hashtags.txt', 'r')]
    query_through_search(tags)
