import numpy as np
from scipy.spatial.distance import cosine
from sklearn.feature_extraction.text import TfidfVectorizer

from src import preprocessing


def lsa(tweets, k):
    summary = []

    vectorizer = TfidfVectorizer(tokenizer=lambda text: preprocessing.tokenize(text, use_stemmer=True))
    matrix = vectorizer.fit_transform(tweets)

    sent2word = matrix.todense()
    word2sent = sent2word.T

    u, d, v = np.linalg.svd(word2sent, full_matrices=True)  # SVD
    sentence_weights = np.array((d ** 2) @ v.T)  # calculate weights

    sorted_scores = list(np.argsort(sentence_weights.squeeze()))

    summary.append(sorted_scores.pop(0))

    for candidate in sorted_scores:
        similarities = [1 - cosine(sent2word[sentence], sent2word[candidate]) for sentence in summary]
        if max(similarities) < 0.6:
            summary.append(candidate)

        if len(summary) == k:
            break
    return summary

