import numpy as np
import networkx
from sklearn.preprocessing import normalize

MAX_RESULTS_CROSS_SIMILARITY = 0.6
DEFAULT_DAMPING_FACTOR = 0.31


def text_rank(distance_matrix, k, d=DEFAULT_DAMPING_FACTOR):
    similarity = 1 - distance_matrix / distance_matrix.max()
    nx_graph = networkx.from_numpy_array(similarity)
    scores = networkx.pagerank(nx_graph, alpha=d)

    ranked_ids = [index for index, score in
                  sorted(scores.items(), key=lambda item: item[1], reverse=True)]

    return get_top_k_diverse_results(ranked_ids, similarity, k)


def my_text_rank(distance_matrix, d, k):
    similarity = 1 - distance_matrix / distance_matrix.max()
    np.fill_diagonal(similarity, 0.0)
    similarity = normalize(similarity, norm='l1', axis=0)

    N = similarity.shape[0]
    v = np.random.rand(N, 1)
    v = v / np.linalg.norm(v, 1)
    last_v = np.full((N, 1), 2.0)

    while np.linalg.norm(v - last_v, 2) > 1.e-10:
        last_v = v
        v = d * np.dot(similarity, v) + (1 - d) / N

    return get_top_k_diverse_results(v.argsort(axis=0)[::-1], similarity, k)


def get_top_k_diverse_results(ranked_ids, similarity, k):
    result = []
    for tweet_id in ranked_ids:
        if np.max(similarity[tweet_id, result], initial=0.0) < MAX_RESULTS_CROSS_SIMILARITY:
            result.append(tweet_id)
            if len(result) >= k:
                break

    return sorted(result)
