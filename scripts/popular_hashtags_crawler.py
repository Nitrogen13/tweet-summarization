import os

import tweepy
from langdetect import detect

from src.constants import CONSUMER_KEY, CONSUMER_SECRET, DATA_PATH

auth = tweepy.AppAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
api = tweepy.API(auth)

trending_topics = api.trends_place(1)

trending_hashtags = [x['name'] for x in trending_topics[0]['trends'] if x['name'].startswith('#')
                     and detect(x['name']) == 'en']

os.makedirs(DATA_PATH / 'raw_tweets', exist_ok=True)
with open(DATA_PATH / 'trending_hashtags.txt', 'w', encoding="UTF-8") as f:
    for hashtag in trending_hashtags:
        f.write(f"{hashtag}\n")
